# Import the frameworks.
from flask import Flask,jsonify,request
import pandas as pd
import statistics

#Create an instance of Flask.
app = Flask(__name__)

# Load the dataset location.csv.
pickup_times = pd.read_csv('/Users/anjuta/Downloads/wolt_databases/pickup_times.csv')

# Create a Rest API with endpoint 'median_pickup_time'.
@app.route('/median_pickup_time')

# This function retrieves three query parameters - location_id, start_time, end_time. 
# It fetches the data from locations.csv dataset where all the values of the parameters meets the condition.
# Create a new dataframe with the new data and calculates the median of the pickup_time.
# Returns the median value in JSON format.
def median_pickup_time():
    try:
        location_id = int(request.args['location_id'])
    except:
        return jsonify({'Error': 'location_id is missing'})
    try:
        start_time = request.args['start_time']
    except:
        return jsonify({'Error': 'start_time is missing'})
    try:
        end_time = request.args['end_time']
    except:
        jsonify({'Error': 'end_time is missing'})

    columns = ('location_id', 'timestamp', 'pickup_times')
    new_df = pd.DataFrame(columns = columns)
    index = 0
    for ind, row in pickup_times.iterrows():
        if (row.location_id == location_id) & (row.iso_8601_timestamp >= start_time) & (row.iso_8601_timestamp <= end_time):
            print('Row location id ', row.location_id)
            new_df.loc[index, ['location_id']] = row.location_id
            new_df.loc[index, ['timestamp']] = row.iso_8601_timestamp
            new_df.loc[index, ['pickup_times']] = row.pickup_time 
            index += 1

    pickup_times_list = new_df['pickup_times'].values
    print('Pickup times list ', pickup_times_list)
    return jsonify({'Median: ': statistics.median(pickup_times_list)})

if __name__ == '__main__':
    app.run(port=4000, debug=True)

